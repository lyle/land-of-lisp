;;;; Chapter 2 - CREATING YOUR FIRST LISP PROGRAM
;;;
;;; The Guess-My-Number Game
;;;
;;; In this game, you pick a number from 1 to 100, and the computer has to
;;;  guess it using the "guess-my-number" function. The computer starts by
;;;  guessing 50, and you enter (smaller) or (bigger) until the computer
;;;  guesses your number.


;;; Our first variables. defparameter defines a global variable,
;;;  and the asterisks --- earmuffs in the Lisp vernacular ---
;;;  are a convention for denoting their global nature. It's not enforced by
;;;  the language, but always do it anyway. Again, embrace the Lisp way.
(defparameter *small* 1)

(defparameter *big* 100)


;;; Values stored with defparameter can be overwritten. If we were to now write:
;;;     (defparameter *big* 50)
;;;  , the computer would only guess from 1 to 50.
;;; To store immutable values, use defvar:
;;;     (defvar *immutable* 999)
;;; Try overwriting a defparameter value in the REPL, then try overwriting a
;;;  defparameter defined value and see what happens.


;;; Global function taking no parameters, serves as the computer's guess
(defun guess-my-number ()
  (ash (+ *small* *big*) -1)) ; ash is an "arithmetic shift" function, a clever
                              ; way of doing a binary search

(defun smaller ()
  (setf *big* (1- (guess-my-number))) ; Set the value of *big* to 1 less than the
  (guess-my-number))                  ; most recent guess, then guess again

(defun bigger ()
  (setf *small* (1+ (guess-my-number))) ; Similarly, increment *small* past the most
  (guess-my-number))                    ; recent guess, then guess again

;; Finally, we need a way to reset those globals to start a new game
(defun start-over ()
  (defparameter *small* 1)
  (defparameter *big* 100) 
  (guess-my-number))
