;;;; Chapter 1 - GETTING STARTED WITH LISP
;;;
;;; Comments in Common Lisp start with the semicolon (;) character.
;;; By convention, file header comments begin with four semicolons
;;;  while comments at the beginning of other lines begin with 3 semicolons.
;;; Embracing the cultural conventions of Lisp at the outset will likely go a
;;;  long way toward achieving enlightenment.
;;;
;;; See README.md for installing and running a Common Lisp interpreter.


;;; Lisp dialects - Uncountable dialects exist which adhere to central
;;;  principles of Lisp. The one this book focuses on is ANSI Common Lisp,
;;;  but the other prominent branch in the Lisp family tree is Scheme.
;;;  Common Lisp tends to emphasize raw power at the expense of being a bit
;;;  ugly due to pragmatic compomises, while Scheme tends toward beauty
;;;  and mathematical purity. Personally, I find Scheme much more pleasing
;;;  to the eye, but that's not what this book is about.
;;;
;;; A middle ground exists in the form of languages such as Haskell, which
;;;  is not technically a Lisp dialect, but hews to the principles central
;;;  to the Lisp way.
;;;
;;; More modern renditions of Lisp include languages such as Arc and Clojure.
;;;
;;; Narrowly focused Lisps exist for scripting within various applications:
;;;  Emacs Lisp (used for scripting the emacs text editor), Guile Scheme
;;;  (used in several open source applications), Script-Fu Scheme (used within
;;;  the GIMP image editor).


(princ "This is how you print strings in Common Lisp.") ; This will be executed immediately
                                                        ; if you run (load "chapter01.lisp")
                                                        ; in the REPL
