Land of Lisp
============
This repo tracks code examples, experiments, and insights from Conrad Barski's [Land of Lisp](http://www.amazon.com/Land-Lisp-Learn-Program-Game/dp/1593272812/ref=sr_1_1?ie=UTF8&qid=1436719947&sr=8-1&keywords=land+of+lisp).

Setting up
==========
The recommended clisp interpreter for doing the examples in this book is [CLISP](http://clisp.cons.org), though others
 will work fine as well. Once installed, you can run `clisp` from the command line to get a REPL. From there, you can
 enter LISP code directly or run code saved in files:

    user@host$ clisp
      i i i i i i i       ooooo    o        ooooooo   ooooo   ooooo
      I I I I I I I      8     8   8           8     8     o  8    8
      I  \ `+' /  I      8         8           8     8        8    8
       \  `-+-'  /       8         8           8      ooooo   8oooo
        `-__|__-'        8         8           8           8  8
            |            8     o   8           8     o     8  8
      ------+------       ooooo    8oooooo  ooo8ooo   ooooo   8
    
    Welcome to GNU CLISP 2.49 (2010-07-07) <http://clisp.cons.org/>
    
    Copyright (c) Bruno Haible, Michael Stoll 1992, 1993
    Copyright (c) Bruno Haible, Marcus Daniels 1994-1997
    Copyright (c) Bruno Haible, Pierpaolo Bernardi, Sam Steingold 1998
    Copyright (c) Bruno Haible, Sam Steingold 1999-2000
    Copyright (c) Sam Steingold, Bruno Haible 2001-2010
    
    Type :h and hit Enter for context help.

    [1]> (+ 1 2)
    3
    [2]>

You can tell this is serious business because of the ASCII art.

Entering Commands
-----------------
You can enter commands directly at the REPL prompt. For example, you could go through each chapter's file
 (e.g. chapter01.lisp) and enter each line individually at the REPL, or you could read the next section and...

Load Files in the REPL
----------------------
To read and evaluate files containing Lisp code, execute `(load "chapter01.lisp")`.
This is useful for running complete programs or loading functions which you can then
manually invoke at the REPL.

Escape Hatch
------------
If you get the REPL into a confused state and you don't know what to do, type `:9` and it'll fix everything.

To exit the repl, run `(quit)`.

Waaah, so many parentheses!
===========================
Be quiet. The only reason you don't complain about semicolons or curly braces is because you're used to them.

The parens are actually really straightforward, and if you follow common formatting style you won't have
any problems. Most worthy text editors will highlight, color-code, and keep track of them for you anyway.
They'll disappear from your notice before you even realize it.

Why you shouldn't learn Lisp
============================
Your current favorite language will quickly resemble
 [Blub](https://en.wikipedia.org/wiki/Paul_Graham_(computer_programmer)#Blub).
If that hasn't happened to you in a while, it comes as quite a shock.
